package services;

import javax.inject.Singleton;

@Singleton
public class ServiceInstances {
        
        private Object service;
        
        private void ServicesInstances(Object service) {
            this.service = service;
        }
        
        public Object getService() {
            return this.service;
        }
        

}
