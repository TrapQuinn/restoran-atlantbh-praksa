package services;

import model.AppUser;
import play.db.jpa.JPA;

public class UserService {
	
    public AppUser getById(int id) {
    	
        return JPA.em().find(AppUser.class, id);
        
    }
    
}
