package repository;

import model.AppUser;
import play.Logger;

public class userRepositoryImpl extends baseRepositoryImpl<AppUser> implements userRepository {
	
	public AppUser saveUser(AppUser user){
		try{
		
		save(user);
		
		}
		catch(Exception e)
		{
			Logger.debug(e.toString());
			throw new IllegalStateException("Upisivanje usera nije uspjelo!");
			
		}
		
		return user;
		
	}
	
	public boolean deleteUser(AppUser user){
		
		try
		{
			delete(user);
		}
		catch(Exception e)
		{
			Logger.debug(e.toString());
			throw new IllegalStateException("Brisanje usera nije uspjelo!");
		}
		return true;
	}

}
