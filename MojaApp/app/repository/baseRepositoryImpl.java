package repository;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.JPA;

public class baseRepositoryImpl<T> implements baseRepository<T> {
	
	//SAVE
	
	public T save(T o){
	
	try{
		
	JPA.em().persist(o);
	
	}
	catch(Exception e)
	{
		throw new IllegalStateException("Upis u bazu nije uspio!");
	}
	
	return o;
	
	}
	
	//FIND
	
	public List<T> find(T o){
		
		List<T> t = new ArrayList<T>();
		
		return t;
	}
	
	
	//DELETE
	
	public boolean delete(T o){
		
	try{	
	JPA.em().remove(o);
	}
	catch(Exception e)
	{
		
		throw new IllegalStateException("Brisanje iz baze nije uspjelo!");
		
	}
		
	return true;
		
	}
	
	//UPDATE
	
	public T update(T o)
	{
		
		
		
		
		return o;
		
	}

}
