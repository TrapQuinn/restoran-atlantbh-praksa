package repository;

import model.AppUser;

public interface userRepository {
	
	public AppUser saveUser(AppUser user);
	public boolean deleteUser(AppUser user);

}
