package repository;

import java.util.List;


public interface baseRepository<T> {

	public T save(T o);
	
	public boolean delete(T o);
	
	public List<T> find(T o);
	
	public T update(T o);
	
}
