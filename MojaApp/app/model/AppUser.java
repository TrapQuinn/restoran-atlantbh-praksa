package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

import model.place.City;


@Entity
@SequenceGenerator(name = "Token_generator", sequenceName = "test_sequence", allocationSize = 1, initialValue = 1)
public class AppUser{
	
	@Id
	//@Column(name="id")
	@GeneratedValue//(strategy = GenerationType.SEQUENCE, generator = "Token_generator")
	private int id;
	//@Column(name = "username")
	private String username;
	private String name;
	private String surname;
	private String password;
	@ManyToOne
	//@JoinColumn(name = "city_id", foreignkey = @ForeignKey(name = "CITY_ID_FK"))
	private City city;
	private String country;
	private boolean admin;
	
	//CONSTRUCTOR
	public AppUser(String username, String name, String surname, String password,/* City city,*/String country, boolean admin) {
		super();
		this.username = username;
		this.name = name;
		this.surname = surname;
		this.password = password;
		//this.city = city;
		this.country = country;
		this.admin = admin;
	}
	
	public AppUser(){
		
		
	}
	
	//SETTERS AND GETTERS
	public int getId() {
		return id;
	}
	/*public void setId(int id) {
		this.id = id;
	}*/
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/*public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}*/
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
}
