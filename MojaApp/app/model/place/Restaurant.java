package model.place;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Restaurant {
	
	
	@Id
	private int id;
	private String name;
	private String address;
	private String phone;
	private String website;
	private String category;
	private String latitude;
	private String longitude;
	private String photos;
	private double workingHours;
	private double reservationCost;
	
	//CONSTRUCTOR
	public Restaurant(int id, String name, String address, String phone, String website, String category, String latitude,
			String longitude, String photos, double workingHours, double reservationCost) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.website = website;
		this.category = category;
		this.latitude = latitude;
		this.longitude = longitude;
		this.photos = photos;
		this.workingHours = workingHours;
		this.reservationCost = reservationCost;
	}
	
	
	//SETTERS AND GETTERS
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getPhotos() {
		return photos;
	}
	public void setPhotos(String photos) {
		this.photos = photos;
	}
	public double getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}
	public double getReservationCost() {
		return reservationCost;
	}
	public void setReservationCost(double reservationCost) {
		this.reservationCost = reservationCost;
	}
	
}
