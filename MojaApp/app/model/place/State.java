package model.place;

import java.util.List;

public class State {
	
	private int id;
	private List<City> cities;
	private String name;
	
	
	//CONSTRUCTOR
	public State(int id, List<City> cities, String name) {
		super();
		this.id = id;
		this.cities = cities;
		this.name = name;
	}
	
	
	//SETTERS AND GETTERS
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<City> getCities() {
		return cities;
	}
	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
}
