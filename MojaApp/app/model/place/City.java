package model.place;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import model.AppUser;

@Entity
//@SequenceGenerator(name = "Token_generator", sequenceName = "test_sequence", allocationSize = 1, initialValue = 1)
public class City {
	
	@Id
	@GeneratedValue
	//@Column(name="city_id")
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Token_generator")
	private int id;
	private String name;
	
	//private List<Restaurant> restaurants;
	
	@OneToMany(mappedBy="city" , cascade=CascadeType.ALL)
	private List<AppUser> appusers = new ArrayList<>();
	
	//CONSTRUCTOR
	public City(String name) {
		super();
		this.name = name;
	}
	
	public City(){
		
	}
	

	//GETTERS AND SETTERS
	public int getId() {
		return id;
	}
	/*public void setId(int id) {
		this.id = id;
	}*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/*public List<Restaurant> getRestaurants() {
		return restaurants;
	}
	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}*/
	
	public List<AppUser> getUsers() {
		return appusers;
	}

	public void setUsers(List<AppUser> users) {
		this.appusers = users;
	}
	
	
	
	
}
