package controllers;

import model.AppUser;
import model.place.City;
import model.place.Restaurant;
import play.mvc.*;
import views.html.*;

import play.db.jpa.Transactional;
import play.libs.Json;
import play.db.jpa.JPA;

import com.fasterxml.jackson.databind.JsonNode;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userRepositoryImpl;
import services.UserService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.lang.Object;
import java.util.ArrayList;
import java.util.List;

import play.Logger;


/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
	
    public Result index(){
        return ok(index.render("Dobro Dosli!"));
    }
    
    @Transactional
    public Result add() {
    	
    	//List<Restaurant> r = new ArrayList<Restaurant>();
    	//List<AppUser> a = new ArrayList<AppUser>();
    	
    	City grad = new City("Sarajevo");
    	AppUser korisnik = new AppUser("Anes","Anes","Anes","Anes","Bosna", true);
    	AppUser korisnik1 = new AppUser("Amar","Amar", "Amar", "Amar" ,"Amar", false);
    	
    	grad.getUsers().add(korisnik);
    	grad.getUsers().add(korisnik1);
    	
    	/*userRepositoryImpl userrepository= new userRepositoryImpl();
    	
    	try{
    	
    	userrepository.save(korisnik);
    	}
    	catch(Exception e)
    	{
    		
    		Logger.debug(e.toString());
    		
    	}*/
    	
    	JPA.em().persist(grad);
    	JPA.em().flush();
    	//Logger.debug("Dodan grad");
    	
       /* AppUser korisnik = new AppUser(2,"Anes", "Anes", "Anes", "Anes", "Anes", "Anes");
        JPA.em().persist(korisnik);
        Logger.debug("Dodan korisnik: "+korisnik);*/
        return ok("Korisnik uspjesno dodan!");
    }
    
    
    @Transactional(readOnly = true)
    public Result listUsers() {
    	
    	/*UserService categoryService = (UserService) ServicesInstances.CATEGORY_SERVICE.getService();
        AppUser category = categoryService.getById(1);
        Logger.debug("Found category: "+category);
        return ok(index.render("Your new application is ready."));*/
    	
    	
    	/*EntityManager em = JPA.em();
    	em.persist(product);
    	em.createQuery("from AppUser").getResultList();*/
    	
    	/*Query query = JPA.em().createQuery("SELECT * FROM public.user");
        List<AppUser> articles = query.getResultList();
        return ok(articles.get(0).getName());*/
    	
        /*CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
        CriteriaQuery<AppUser> cq = cb.createQuery(AppUser.class);
        Root<AppUser> root = cq.from(AppUser.class);
        CriteriaQuery<AppUser> all = cq.select(root);
        TypedQuery<AppUser> allQuery = JPA.em().createQuery(all);
        JsonNode jsonNodes = Json.toJson(allQuery.getResultList());
        return ok(jsonNodes);*/
    	return ok("Evo me");
    }
    
}
